# Timer
A simple timer application that prints updates periodically and notifies you when the time is up.

This is basically a wrapper around `sleep foo; notify-send bar` with output to let you know how far you are.

## Installing
Clone this repo, then run
```shell
cargo build --release
cp target/release/timer $HOME/bin/
```
to build the executable and copy it to a directory in your `$PATH` (replace `$HOME/bin/` with a location on your system).
