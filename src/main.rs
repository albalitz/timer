use std::process::exit;
use std::thread::sleep;
use std::time::Duration;

use clap::{crate_name, crate_version};
use clap::{App, Arg};
use notify_rust::Notification;
use pbr::ProgressBar;

fn parse_duration(mut duration_s: String) -> Result<Duration, String> {
    //! Parse a string to a duration.
    //! e.g. 1m -> Duration::from_secs(60)
    let plain_number_duration = duration_s.parse::<u64>();
    if plain_number_duration.is_ok() {
        return Ok(Duration::from_secs(plain_number_duration.unwrap()));
    }

    if duration_s.chars().all(|c| c.is_alphabetic()) {
        return Err("Duration may not be an empty string!".to_string());
    }
    let suffix = duration_s.split_off(duration_s.len() - 1);
    let seconds: u64 = match duration_s.parse::<u64>() {
        Ok(s) => s,
        Err(e) => return Err(e.to_string()),
    };
    let multiplicator = match suffix.as_str() {
        "s" => 1,
        "m" => 60,
        "h" => 60 * 60,
        "d" => 60 * 60 * 24,
        _ => return Err(format!("Not a valid suffix: {}", suffix)),
    };

    Ok(Duration::from_secs(seconds * multiplicator))
}

fn sleep_print_loop(mut duration: Duration) {
    //! Sleep for the given duration, printing periodic updates
    let mut progress_bar = ProgressBar::new(duration.as_secs());
    progress_bar.show_speed = false;
    progress_bar.set(0);

    while duration.as_secs() > 0 {
        let one_second = Duration::from_secs(1);
        sleep(one_second);
        duration -= one_second;
        progress_bar.inc();
    }
}

fn notify(message: String) {
    // TODO: .body(body)
    Notification::new()
        .summary(message.as_str())
        .show()
        .unwrap();
    // TODO: `println!` ?
}

fn main() {
    const APP_NAME: &str = crate_name!();
    const APP_VERSION: &str = crate_version!();
    const ABOUT: &str = "CLI for simple budget management";

    let args = App::new(APP_NAME)
        .version(APP_VERSION)
        .about(ABOUT)
        .arg(Arg::with_name("duration").help("Count down the given duration.
A suffix of 's', 'm', 'h', or 'd' may be used to specify the duration as seconds, minutes, hours, or days respectively. Defaults to seconds when no suffix is given.").required(true))
        .arg(Arg::with_name("message").help("A custom message with which you are notified").default_value("Time is up!").required(false))
        .get_matches();

    let duration_arg: String = args.value_of("duration").unwrap().to_string();
    let duration: Duration = match parse_duration(duration_arg) {
        Ok(d) => d,
        Err(e) => {
            eprintln!("Error parsing duration: {}", e);
            exit(1);
        }
    };
    sleep_print_loop(duration);
    notify(args.value_of("message").unwrap().to_string());
}

#[cfg(test)]
mod test_parse_duration {
    use super::*;

    #[test]
    fn should_parse_numbers_as_seconds() {
        assert_eq!(
            parse_duration("42".to_string()),
            Ok(Duration::from_secs(42))
        );
    }

    #[test]
    fn should_parse_number_with_s_as_seconds() {
        assert_eq!(
            parse_duration("13s".to_string()),
            Ok(Duration::from_secs(13))
        );
    }

    #[test]
    fn should_parse_number_with_m_as_minutes() {
        assert_eq!(
            parse_duration("37m".to_string()),
            Ok(Duration::from_secs(37 * 60))
        );
    }

    #[test]
    fn should_parse_number_with_h_as_hours() {
        assert_eq!(
            parse_duration("4h".to_string()),
            Ok(Duration::from_secs(4 * 60 * 60))
        );
    }

    #[test]
    fn should_parse_number_with_d_as_days() {
        assert_eq!(
            parse_duration("8d".to_string()),
            Ok(Duration::from_secs(8 * 60 * 60 * 24))
        );
    }

    #[test]
    fn should_not_accept_unknown_suffix() {
        assert!(parse_duration("8x".to_string()).is_err(),);
    }

    #[test]
    fn should_not_accept_letters_only() {
        assert!(parse_duration("x".to_string()).is_err(),);
    }

    #[test]
    fn should_not_accept_empty_strings() {
        assert!(parse_duration("".to_string()).is_err(),);
        assert!(parse_duration(" ".to_string()).is_err(),);
        assert!(parse_duration("  ".to_string()).is_err(),);
        assert!(parse_duration("   ".to_string()).is_err(),);
    }
}
